# Open VPN setup on CentOS 8 on Digital Ocean with Floating IP:

This article shows how to setup OpenVPN on a DigitalOcean VM/droplet. The Software installation and configuration of OpenVPN is generally very straight-forward - if you are *not* using a floating-ip. This article was written to handle the complication of using a floating-ip.


For any DigitalOcean instance, as soon as you create it, the instance (or droplet) gets three IP addresses. If you additionally assign a floating IP address to your instance then we have four IP addresses related to one DigitalOcean virtual machine or "droplet" .

These IP addresses are:

* primary public IP - (or "droplet-ip") - `eth0`: `159.223.78.228`
* anchor IP - (or "anchor-ip") - `eth0`: `10.15.0.5` 
* primary private IP - (or "private-ip") - `eth1`: `10.104.0.2`
* (additional/optional) floating IP - (or "floating-ip"): `144.126.242.84`

| ![images/diagram.png](images/diagram.png) |
| ------------------------------------------|


**Notes:**
* Anchor IP is always tied to the primary public IP of the VM/droplet, and appears as *secondary IP* on the same network interface where the public IP is assigned as the *primary IP*. e.g. The `eth0` interface throughout this document has public IP as *primary IP* and anchor IP as *secondary IP*.  
* Sometimes the "anchor IP" and the "primary private IP" may seem to be *similar*, but they are always different, and belong to two different private networks.
* The floating IP (when used) is not assigned to any interface on VM/droplet itself. It is managed by the service provider within the provider's network, and is tied to the anchor IP using NAT mechanism.


As soon as you setup VPN software and the service is running, then you will have another interface `tun0` and another (private) IP address on that interface. In case of default OpenVPN configuration, this is most probably be `10.8.0.1`.

```
[root@SERVER1 ~]# ifconfig
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 159.223.78.228  netmask 255.255.240.0  broadcast 159.223.79.255   <------ Primary IP - "Public"
        inet6 fe80::448b:40ff:fe3d:a8b6  prefixlen 64  scopeid 0x20<link>
        ether 46:8b:40:3d:a8:b6  txqueuelen 1000  (Ethernet)
        RX packets 70173  bytes 54270587 (51.7 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 52978  bytes 9599297 (9.1 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

eth1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 10.104.0.2  netmask 255.255.240.0  broadcast 10.104.15.255     <------ Primary IP - "Private"
        inet6 fe80::a43a:21ff:fe9b:f29  prefixlen 64  scopeid 0x20<link>
        ether a6:3a:21:9b:0f:29  txqueuelen 1000  (Ethernet)
        RX packets 27  bytes 1966 (1.9 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 31  bytes 2182 (2.1 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 6  bytes 416 (416.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 6  bytes 416 (416.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

tun0: flags=4305<UP,POINTOPOINT,RUNNING,NOARP,MULTICAST>  mtu 1500
        inet 10.8.0.1  netmask 255.255.255.0  destination 10.8.0.1     <------ OpenVPN IP - "Private"
        inet6 fe80::a5ee:ffc6:50fb:bd7e  prefixlen 64  scopeid 0x20<link>
        unspec 00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00  txqueuelen 100  (UNSPEC)
        RX packets 2634  bytes 611467 (597.1 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 2563  bytes 644342 (629.2 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

[root@SERVER1 ~]# 
```

If you run `ip addr show` command, *only then* the anchor IP will show up as secondary IP on that interface. In the output below, it shows up under `eth0` interface, along the droplet's primary public IP. So, to see all IP addresses of a VM always use `ip addr show` command. The `ifconfig` command has limitations as it can only show the primary/main IP and any IPs on sub-interfaces - but not secondary IPs.

```
[root@SERVER1 ~]# ip addr show
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 46:8b:40:3d:a8:b6 brd ff:ff:ff:ff:ff:ff
    inet 159.223.78.228/20 brd 159.223.79.255 scope global noprefixroute eth0    <------ Primary IP - "Public"
       valid_lft forever preferred_lft forever
    inet 10.15.0.5/16 brd 10.15.255.255 scope global noprefixroute eth0     <------ Secondary IP (anchor IP) - "Private"
       valid_lft forever preferred_lft forever
    inet6 fe80::448b:40ff:fe3d:a8b6/64 scope link 
       valid_lft forever preferred_lft forever
3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether a6:3a:21:9b:0f:29 brd ff:ff:ff:ff:ff:ff
    inet 10.104.0.2/20 brd 10.104.15.255 scope global noprefixroute eth1     <------ Primary IP - "Private" 
       valid_lft forever preferred_lft forever
    inet6 fe80::a43a:21ff:fe9b:f29/64 scope link 
       valid_lft forever preferred_lft forever
34: tun0: <POINTOPOINT,MULTICAST,NOARP,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UNKNOWN group default qlen 100
    link/none 
    inet 10.8.0.1/24 brd 10.8.0.255 scope global tun0     <------ OpenVPN IP - "Private"
       valid_lft forever preferred_lft forever
    inet6 fe80::a5ee:ffc6:50fb:bd7e/64 scope link stable-privacy 
       valid_lft forever preferred_lft forever
[root@SERVER1 ~]# 
```


## Lets verify this information: 
You can query the metadata service (always) available on each host. This is a service is provided by default on (almost?) all cloud providers.

```
[root@SERVER1 ~]# curl -s http://169.254.169.254/metadata/v1/interfaces/public/0/ipv4/address

159.223.78.228

[root@SERVER1 ~]# curl -s http://169.254.169.254/metadata/v1/interfaces/public/0/anchor_ipv4/address

10.15.0.5


[root@SERVER1 ~]# curl -s http://169.254.169.254/metadata/v1/interfaces/private/0/ipv4/address

10.104.0.2


[root@SERVER1 ~]# curl -s http://169.254.169.254/metadata/v1/floating_ip/ipv4/ip_address
144.126.242.84


[root@SERVER1 ~]# curl -s http://169.254.169.254/metadata/v1/floating_ip/ipv4/active

true
```

## Download the OpenVPN setup script:

```
[root@SERVER1 iptables]# wget https://raw.githubusercontent.com/Angristan/openvpn-install/master/openvpn-install.sh -O centos-8-vpn.sh
```

```
chmod +x centos-8-vpn.sh
```


## Run the setup script:
Notes:
* The first time it asks you for IP address, provide the value of "anchor-ip".
* The second time it asks you for Public IP address, provide the value of "floating-ip".

```
[root@SERVER1 iptables]# /root/centos-8-vpn.sh 
Welcome to the OpenVPN installer!
The git repository is available at: https://github.com/angristan/openvpn-install

I need to ask you a few questions before starting the setup.
You can leave the default options and just press enter if you are ok with them.

I need to know the IPv4 address of the network interface you want OpenVPN listening to.
Unless your server is behind NAT, it should be your public IPv4 address.
IP address: 10.15.0.5

It seems this server is behind NAT. What is its public IPv4 address or hostname?
We need it for the clients to connect to the server.
Public IPv4 address or hostname: 144.126.242.84

Checking for IPv6 connectivity...

Your host does not appear to have IPv6 connectivity.

Do you want to enable IPv6 support (NAT)? [y/n]: n

What port do you want OpenVPN to listen to?
   1) Default: 1194
   2) Custom
   3) Random [49152-65535]
Port choice [1-3]: 1

What protocol do you want OpenVPN to use?
UDP is faster. Unless it is not available, you shouldn't use TCP.
   1) UDP
   2) TCP
Protocol [1-2]: 1

What DNS resolvers do you want to use with the VPN?
   1) Current system resolvers (from /etc/resolv.conf)
   2) Self-hosted DNS Resolver (Unbound)
   3) Cloudflare (Anycast: worldwide)
   4) Quad9 (Anycast: worldwide)
   5) Quad9 uncensored (Anycast: worldwide)
   6) FDN (France)
   7) DNS.WATCH (Germany)
   8) OpenDNS (Anycast: worldwide)
   9) Google (Anycast: worldwide)
   10) Yandex Basic (Russia)
   11) AdGuard DNS (Anycast: worldwide)
   12) NextDNS (Anycast: worldwide)
   13) Custom
DNS [1-12]: 9

Do you want to use compression? It is not recommended since the VORACLE attack makes use of it.
Enable compression? [y/n]: n

Do you want to customize encryption settings?
Unless you know what you're doing, you should stick with the default parameters provided by the script.
Note that whatever you choose, all the choices presented in the script are safe. (Unlike OpenVPN's defaults)
See https://github.com/angristan/openvpn-install#security-and-encryption to learn more.

Customize encryption settings? [y/n]: n

Okay, that was all I needed. We are ready to setup your OpenVPN server now.
You will be able to generate a client at the end of the installation.
Press any key to continue...
. . . 
```

The script continues running. At the end, the script asks for creating a user, which you should create. Then you need to download the `.ovpn` file it creates to your local computer.


```
. . . 

Tell me a name for the client.
The name must consist of alphanumeric character. It may also include an underscore or a dash.
Client name: user5

Do you want to protect the configuration file with a password?
(e.g. encrypt the private key with a password)
   1) Add a passwordless client
   2) Use a password for the client
Select an option [1-2]: 1

. . . 

Client user5 added.

The configuration file has been written to /root/user5.ovpn.
Download the .ovpn file and import it in your OpenVPN client.

[root@SERVER1 ~]#
```

From another terminal from your home computer, download the `user5.ovpn` file. 

```
[kamran@kworkhorse ~]$ scp  root@159.223.78.228:/root/user5.ovpn  /home/kamran/tmp/
```

We will use the file in a moment.

Next, modify the `/etc/openvpn/server.conf` file and add `local <anchor-ip>` to the top of the file, as shown below:

```
[root@SERVER1 ~]# cat /etc/openvpn/server.conf 
local 10.15.0.5
port 1194
proto udp
dev tun
user nobody
group nobody
persist-key
persist-tun
keepalive 10 120
topology subnet
server 10.8.0.0 255.255.255.0
ifconfig-pool-persist ipp.txt
push "dhcp-option DNS 8.8.8.8"
push "dhcp-option DNS 8.8.4.4"
push "redirect-gateway def1 bypass-dhcp"
dh none
ecdh-curve prime256v1
tls-crypt tls-crypt.key
crl-verify crl.pem
ca ca.crt
cert server_raMETpqp33Y2WV8K.crt
key server_raMETpqp33Y2WV8K.key
auth SHA256
cipher AES-128-GCM
ncp-ciphers AES-128-GCM
tls-server
tls-version-min 1.2
tls-cipher TLS-ECDHE-ECDSA-WITH-AES-128-GCM-SHA256
client-config-dir /etc/openvpn/ccd
status /var/log/openvpn/status.log
verb 3
[root@SERVER1 ~]# 
```

## Restart OpenVPN service:

```
systemctl restart openvpn-server@server.service
```


```
systemctl status openvpn-server@server.service
```


## Network interfaces and IP addresses after VPN software is setup and running:

```
[root@SERVER1 ~]# ip addr show
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 46:8b:40:3d:a8:b6 brd ff:ff:ff:ff:ff:ff
    inet 159.223.78.228/20 brd 159.223.79.255 scope global noprefixroute eth0
       valid_lft forever preferred_lft forever
    inet 10.15.0.5/16 brd 10.15.255.255 scope global noprefixroute eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::448b:40ff:fe3d:a8b6/64 scope link 
       valid_lft forever preferred_lft forever
3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether a6:3a:21:9b:0f:29 brd ff:ff:ff:ff:ff:ff
    inet 10.104.0.2/20 brd 10.104.15.255 scope global noprefixroute eth1
       valid_lft forever preferred_lft forever
    inet6 fe80::a43a:21ff:fe9b:f29/64 scope link 
       valid_lft forever preferred_lft forever
34: tun0: <POINTOPOINT,MULTICAST,NOARP,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UNKNOWN group default qlen 100
    link/none 
    inet 10.8.0.1/24 brd 10.8.0.255 scope global tun0
       valid_lft forever preferred_lft forever
    inet6 fe80::a5ee:ffc6:50fb:bd7e/64 scope link stable-privacy 
       valid_lft forever preferred_lft forever
[root@SERVER1 ~]# 
```

Here is the routing table for extra information:
```
[root@SERVER1 ~]# route -n
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
0.0.0.0         159.223.64.1    0.0.0.0         UG    100    0        0 eth0
10.8.0.0        0.0.0.0         255.255.255.0   U     0      0        0 tun0
10.15.0.0       0.0.0.0         255.255.0.0     U     100    0        0 eth0
10.104.0.0      0.0.0.0         255.255.240.0   U     101    0        0 eth1
159.223.64.0    0.0.0.0         255.255.240.0   U     100    0        0 eth0
[root@SERVER1 ~]# 
```


## Setup IP tables correctly to work with floating IP:

Comment out the first POSTROUTING MASQUERADE rule from both files, and change it to  SNAT using the anchor-ip as shown below:

```
root@SERVER1 ~]# cat /etc/iptables/rm-openvpn-rules.sh
#!/bin/sh
#iptables -t nat -D POSTROUTING -s 10.8.0.0/24 -o eth0 -j MASQUERADE
iptables -t nat -D POSTROUTING -s 10.8.0.0/24 -j SNAT --to-source 10.15.0.5
iptables -D INPUT -i tun0 -j ACCEPT
iptables -D FORWARD -i eth0 -o tun0 -j ACCEPT
iptables -D FORWARD -i tun0 -o eth0 -j ACCEPT
iptables -D INPUT -i eth0 -p udp --dport 1194 -j ACCEPT
```

```
[root@SERVER1 ~]# cat /etc/iptables/add-openvpn-rules.sh
#!/bin/sh
# iptables -t nat -I POSTROUTING 1 -s 10.8.0.0/24 -o eth0 -j MASQUERADE
iptables -t nat -I POSTROUTING 1 -s 10.8.0.0/24 -j SNAT --to-source 10.15.0.5
iptables -I INPUT 1 -i tun0 -j ACCEPT
iptables -I FORWARD 1 -i eth0 -o tun0 -j ACCEPT
iptables -I FORWARD 1 -i tun0 -o eth0 -j ACCEPT
iptables -I INPUT 1 -i eth0 -p udp --dport 1194 -j ACCEPT
[root@SERVER1 ~]# 
```

Next flush all IPTables rules:

```
iptables -F ; iptables -t nat -F
```

Next, run the add-openvpn-rules.sh script:

```
/etc/iptables/add-openvpn-rules.sh
```

You can also use the special iptables service setup by the setup script, but it is important to flush the iptables rules completely before you do that.

```
iptables -F ; iptables -t nat -F

systemctl restart iptables-openvpn.service
```

## Setup VPN client software on the client computer:

Import the `user5.ovpn` file you copied a moment ago into a VPN configuration tool on your OS. I am using Fedora, so I simply go to `Settings -> Network -> VPN -> Import file -> user5.ovpn`.

![images/clien-vpn-1.png](images/client-vpn-1.png)

![images/clien-vpn-2.png](images/client-vpn-2.png)

![images/clien-vpn-3.png](images/client-vpn-3.png)

![images/clien-vpn-4.png](images/client-vpn-4.png)


On the client computer you should be able to see a new network interface `tun0` with an IP from `10.8.0.0/24` subnet. In my case it is `10.8.0.2`. 

```
[kamran@kworkhorse ~]$ ip addr show
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever

2: wlp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether 34:f3:9a:27:e7:2d brd ff:ff:ff:ff:ff:ff
    inet 192.168.0.52/24 brd 192.168.0.255 scope global dynamic noprefixroute wlp2s0
       valid_lft 3459sec preferred_lft 3459sec
    inet6 fe80::d607:5226:ed09:edeb/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever

78: tun0: <POINTOPOINT,MULTICAST,NOARP,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UNKNOWN group default qlen 500
    link/none 
    inet 10.8.0.2/24 brd 10.8.0.255 scope global noprefixroute tun0
       valid_lft forever preferred_lft forever
    inet6 fe80::41d0:3d89:bf31:1533/64 scope link stable-privacy 
       valid_lft forever preferred_lft forever
```


```
[kamran@kworkhorse ~]$ route -n
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
0.0.0.0         192.168.0.1     0.0.0.0         UG    600    0        0 wlp2s0
10.8.0.0        0.0.0.0         255.255.255.0   U     50     0        0 tun0
144.126.242.84  192.168.0.1     255.255.255.255 UGH   600    0        0 wlp2s0
[kamran@kworkhorse ~]$ 
```



From the client computer, I can now access the private interface of the remote VPN server, and access services being served from there.

```
[kamran@kworkhorse ~]$ ping 10.8.0.1
PING 10.8.0.1 (10.8.0.1) 56(84) bytes of data.
64 bytes from 10.8.0.1: icmp_seq=1 ttl=64 time=304 ms
64 bytes from 10.8.0.1: icmp_seq=2 ttl=64 time=328 ms
^C
```


```
[kamran@kworkhorse ~]$ sftp aff-bni@10.8.0.1
Connected to 10.8.0.1.
sftp> pwd
Remote working directory: /
sftp> ls -l
drwx------    2 aff-bni  1002           39 Dec 29 18:00 files
sftp> quit
[kamran@kworkhorse ~]$ 
```


------

# Appendix:

## Another example of a DigitalOcean VM / droplet without OpenVPN setup:

The example below is a simple web server running as a droplet on DigitalOcean. To see all IP addresses of a VM always use `ip addr show` command. The `ifconfig` command has limitations.

In the example below, we can see that:
* primary public IP is: `165.227.224.189/20`
* anchor IP is: `10.16.0.5/16`
* primary private IP is: `10.106.0.2/20`
* There is no floating IP assigned to this VM.

```
[root@server1-digitalocean ~]# ip addr show
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 12:17:8a:b1:8b:d0 brd ff:ff:ff:ff:ff:ff
    altname enp0s3
    altname ens3
    inet 165.227.224.189/20 brd 165.227.239.255 scope global noprefixroute eth0
       valid_lft forever preferred_lft forever
    inet 10.16.0.5/16 brd 10.16.255.255 scope global noprefixroute eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::1017:8aff:feb1:8bd0/64 scope link 
       valid_lft forever preferred_lft forever
3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 8a:04:08:b9:76:df brd ff:ff:ff:ff:ff:ff
    altname enp0s4
    altname ens4
    inet 10.106.0.2/20 brd 10.106.15.255 scope global noprefixroute eth1
       valid_lft forever preferred_lft forever
    inet6 fe80::8804:8ff:feb9:76df/64 scope link 
       valid_lft forever preferred_lft forever
```

Here is the routing table for extra information:

```
[root@server1-digitalocean ~]# route -n
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
0.0.0.0         165.227.224.1   0.0.0.0         UG    100    0        0 eth0
10.16.0.0       0.0.0.0         255.255.0.0     U     100    0        0 eth0
10.106.0.0      0.0.0.0         255.255.240.0   U     101    0        0 eth1
165.227.224.0   0.0.0.0         255.255.240.0   U     100    0        0 eth0
[root@server1-digitalocean ~]# 
```

------
References:
* [https://www.cyberciti.biz/faq/centos-8-set-up-openvpn-server-in-5-minutes/](https://www.cyberciti.biz/faq/centos-8-set-up-openvpn-server-in-5-minutes/)
* [https://blog.programster.org/openvpn-digitalocean-ip-alias](https://blog.programster.org/openvpn-digitalocean-ip-alias)
* [https://github.com/angristan/openvpn-install/issues/360](https://github.com/angristan/openvpn-install/issues/360)
