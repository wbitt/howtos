# SFTP server setup with Key-based authentication

**Question:** Why not use simple password based authentication with long passwords?

**Answer:** Because password based authentication is just 1FA (One Factor Authentication). If you lose the password, or if it gets leaked, then all is gone. Whereas key based authentication uses/provides 2FA (Two Factor Authentication) *when* protected by a **passphrase**. If you are using SSH (or any of the sub services), then you **must** use key based authentication.


## SSH Keys:
Before starting, you should have a SSH keypair in your possession. If not, you can generate one. Just be sure that you do assign it a passphrase, which is easy for you to remember, but difficult for anyone to guess, and long enough so it does not break when brute force is applied. The passphrase encrypts the private key as a security measure.

### Generate SSH keypair on Windows using `Putty`:

Putty generates two files by default:
* PPK file (the private part of they key - in Putty format)
* PUB file (the public part of the key - in txt format)

To be able to access a Linux server over SSH or SFTP, you will need the public key in OpenSSH format, which is automatically made avaiable to you in the same putty window, where you are generating the keys. You will need to manually copy the text from that textbox and save it as a txt file, such as `mykey.pub`.

![https://putty.org.ru/img/putty-ssh-key-auth-1.png](https://putty.org.ru/img/putty-ssh-key-auth-1.png)

![https://d33wubrfki0l68.cloudfront.net/79be3e5a7a760646a057d6b6047c1fab021cd930/73097/how-to/log-into-a-linux-server-with-an-ssh-private-key-on-windows/windows9.png](https://d33wubrfki0l68.cloudfront.net/79be3e5a7a760646a057d6b6047c1fab021cd930/73097/how-to/log-into-a-linux-server-with-an-ssh-private-key-on-windows/windows9.png)

![https://devops.ionos.com/tutorials/static/img/tutorials/linux/putty_ssh_auth.png](https://devops.ionos.com/tutorials/static/img/tutorials/linux/putty_ssh_auth.png)


### Generate SSH keypair on Windows using `bash`:
You can install `git` on your local windows PC, which gives you a bash terminal, with look and feel (and tools) of a Linux environment. Once you have it installed, open the bash terminal on windows, and follow the instructions from the Linux section - next.



### Generate SSH keypair on Linux , Mac, Windows bash or Windows subsystem for Linux:

On your local work computer, open shell terminal, and run the following command. Make sure to assign a passphrase, and make a note of it at a secure location.

```
ssh-keygen -t rsa
```

Complete command in action:
```
[student@kworkhorse ~]$ ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/home/student/.ssh/id_rsa): 
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/student/.ssh/id_rsa
Your public key has been saved in /home/student/.ssh/id_rsa.pub
The key fingerprint is:
SHA256:eSimXRTOlONsuKAd+aW73L2G7RHfJmxhIyoj/fJXOXo student@kworkhorse.oslo.praqma.com
The key's randomart image is:
+---[RSA 3072]----+
|        o.       |
|       +o.       |
|     . ++.       |
|    + ..=o       |
|   o +o=S + +.   |
|  . .=+o o *++   |
|    o =..o.o=.o  |
|     oo=..=oEo   |
|      o++++o     |
+----[SHA256]-----+
```

```
[student@kworkhorse ~]$ ls -la .ssh/
total 16
drwx------ 2 student student 4096 Dec 27 00:33 .
drwx------ 7 student student 4096 Nov  1  2020 ..
-rw------- 1 student student 2675 Dec 27 00:33 id_rsa
-rw-r--r-- 1 student student  588 Dec 27 00:33 id_rsa.pub
[student@kworkhorse ~]$ 
```


or even stronger:

```
ssh-keygen -t ed25519
```


Complete command in action:
```
[student@kworkhorse ~]$ ssh-keygen -t ed25519
Generating public/private ed25519 key pair.
Enter file in which to save the key (/home/student/.ssh/id_ed25519):         
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/student/.ssh/id_ed25519
Your public key has been saved in /home/student/.ssh/id_ed25519.pub
The key fingerprint is:
SHA256:YLePzQC4cqpyhlkwxCABNuEyVKcrg2u8fxYabkDToHk student@kworkhorse.oslo.praqma.com
The key's randomart image is:
+--[ED25519 256]--+
|O*o. .           |
|=+. o.           |
|=oo.. + .        |
|B+E..o + .       |
|o*o.o   S        |
|.o+= .   *       |
|.*+ o . . +      |
|=.++ o           |
|o+o.o            |
+----[SHA256]-----+
```

```
[student@kworkhorse ~]$ ls -la .ssh/
total 24
drwx------ 2 student student 4096 Dec 27 00:34 .
drwx------ 7 student student 4096 Nov  1  2020 ..
-rw------- 1 student student  484 Dec 27 00:34 id_ed25519
-rw-r--r-- 1 student student  116 Dec 27 00:34 id_ed25519.pub
-rw------- 1 student student 2675 Dec 27 00:33 id_rsa
-rw-r--r-- 1 student student  588 Dec 27 00:33 id_rsa.pub
[student@kworkhorse ~]$ 
```

Above command(s) will generate the SSH keypair in openssh format.

### A note about FileZilla:

By default FileZilla will try to use the private key from default locations depending on which OS you are on, and which server you are trying to connect to. However, if you manually create "FTP sites" in the FileZilla menu, and you want to use key based authentication, then FileZilla expects the private key to be in PPK format (even though it may be running on Linux - which is silly). So if you generated your SSH keypair on Linux and are going to use FileZilla, then you may have to perform an additional step of converting your private key file from OpenSSH format to PPK format.

On windows you use puttygen to load/import the private key in openssh format, and then save it again as PPK file.

On Linux, you first need to install `putty` or `putty-tools` , then use the following command to convert your private key file from openssh format to PPK:

```
[root@SFTP-SERVER ~]# yum install putty
```

```
puttygen id_rsa -o id_rsa.ppk
```

Detailed output:

```
[student@kworkhorse ~]$ puttygen .ssh/id_rsa  -o .ssh/id_rsa.ppk
Enter passphrase to load key: 
```

```
[student@kworkhorse ~]$ ls -l .ssh
total 20
-rw------- 1 student student  484 Dec 27 00:34 id_ed25519
-rw-r--r-- 1 student student  116 Dec 27 00:34 id_ed25519.pub
-rw------- 1 student student 2675 Dec 27 00:33 id_rsa
-rw------- 1 student student 2239 Dec 27 00:58 id_rsa.ppk
-rw-r--r-- 1 student student  588 Dec 27 00:33 id_rsa.pub
[student@kworkhorse ~]$ 
```
 
## Generate SSH keypair for AFF admin:
On AFF admin's work computer (windows):
* Create a SSH keypair (RSA) using putty-keygen. Ensure to assign a passphrase to the key. 
* Find the open-ssh format public key in the same putty-keygen window.
* Send this openssh public key to server admin, using email.

```
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDgRsMnS/xJ3D7ZWFWUWe4/T7iPFSrgCKeh+W8McZ1loXPW2b1BxH5HtY4md7eB39gaR+oYdp9Ov5PNU09UAndWxM+ky71QEol4QtEqdbZ0VdVfDvWor3Ed8yjXCsk9z7lCHiLNu+uTE8j0BpB77aDcasns5atDpuLADgLrAa/pvf5x0iB5JZx/MLBH3qwGQqxbBmotFj+aBkpwgYeDk7p6I/huNd7FIHoEA8gn19pGiYRSeLsp6yRXr0e3QLf4f1e6pYz8dFIufqjwRz6uGrT5FyyF9ZEbcCCbM+NelRH5SIzZBxzWRQoaawP86O3s6Mpft/aEEO0fj+diB/s0fazP AFF-CLD-SFTP-Key1
```

(Above is a public key, and it is very ok and safe to public your public key over public internet.)


## Generate SSH keypair for each of your client:
Ask each of the client or the responsible person for your clients to generate SSH keypair for themselves, as described in the sections above, and then ask them to send you the public key in openssh format , over email.


## Server setup:
A CENTOS 8 based VM on Digital Ocean, with:
* OS disk (25 GB)
* Data disk (10 GB), or whatever your requirement is.
* Public IP


### Disable SELinux:

```
[root@SFTP-SERVER ~]# vi /etc/selinux/config

SELINUX=disabled
SELINUXTYPE=targeted
```

### Disable firewalld, iptables:
```
[root@SFTP-SERVER ~]# systemctl stop firewalld

[root@SFTP-SERVER ~]# systemctl stop iptables

[root@SFTP-SERVER ~]# systemctl disable firewalld

[root@SFTP-SERVER ~]# systemctl disable iptables
```

### Update and reboot:
```
[root@SFTP-SERVER ~]# yum -y update && reboot
```


### Add your SSH pub key to `/root/.ssh/authorized_keys`:

Add System admin's SSH public key to /root/.ssh/authorized_keys
```
[root@SFTP-SERVER ~]# vi /root/.ssh/authorized_keys
```
(add your pub key here)

Verify that you can login as root using your ssh key.


## Disable password based authentication in SSHD:
Once you can login as root using key and passphrase, NOT password, then it is time to disable password based authentication in SSHD configuration.

```
[root@SFTP-SERVER ~]# vi /etc/ssh/sshd_config 
. . . 
PasswordAuthentication no
. . . 

[root@SFTP-SERVER ~]# systemctl restart sshd
```

Verify that server does not fall back to password based authentication:


```
[kamran@kworkhorse ~]$ ssh -o pubkeyauthentication=no  root@46.101.53.95
root@46.101.53.95: Permission denied (publickey,gssapi-keyex,gssapi-with-mic).
[kamran@kworkhorse ~]$ 
```

This is good!


```
[kamran@kworkhorse ~]$ ssh -o passwordauthentication=yes -o pubkeyauthentication=no root@46.101.53.95
root@46.101.53.95: Permission denied (publickey,gssapi-keyex,gssapi-with-mic).
[kamran@kworkhorse ~]$ 
```

This is good!



# Step 1 - Basic SSH and SFTP - without chroot:

## On server - User setup:

```
[root@SFTP-SERVER ~]# useradd -c "aff-demo1 SFTP account" -m aff-demo1

[root@SFTP-SERVER ~]# useradd -c "aff-demo2 SFTP account" -m aff-demo2

[root@SFTP-SERVER ~]# useradd -c "aff-demo3 SFTP account" -m aff-demo3

[root@SFTP-SERVER ~]# mkdir /home/aff-demo{1,2,3}/.ssh

[root@SFTP-SERVER ~]# chmod 0700 /home/aff-demo{1,2,3}/.ssh 
```


Add the public key received by email and add it to each user's .ssh/authorized keys on the server.

```
[root@SFTP-SERVER ~]# chown aff-demo1:aff-demo1 /home/aff-demo1 -R

[root@SFTP-SERVER ~]# chown aff-demo2:aff-demo2 /home/aff-demo2 -R

[root@SFTP-SERVER ~]# chown aff-demo3:aff-demo3 /home/aff-demo3 -R 
```


## Verify connection using SSH:

Try using putty from windows to connect to the server using the user accounts created earlier on the server. You have to provide path to the private part of SSH key (.ppk), under `Connection -> SSH -> Auth` settings.


## Verify connection using SFTP:

```
[kamran@kworkhorse ~]$ sftp  aff-demo1@46.101.53.95
Connected to 46.101.53.95.
sftp> ls -la
drwx------    3 aff-demo1 aff-demo1      190 Dec 26 20:39 .
drwxr-xr-x    6 root     root           71 Dec 26 20:14 ..
-rw-------    1 aff-demo1 aff-demo1       14 Dec 26 20:24 .bash_history
-rw-r--r--    1 aff-demo1 aff-demo1       18 Jul 27 14:21 .bash_logout
-rw-r--r--    1 aff-demo1 aff-demo1      141 Jul 27 14:21 .bash_profile
-rw-r--r--    1 aff-demo1 aff-demo1      376 Jul 27 14:21 .bashrc
-rw-r--r--    1 aff-demo1 aff-demo1        0 Dec 26 20:05 .cloud-locale-test.skip
drwx------    2 aff-demo1 aff-demo1       29 Dec 26 20:37 .ssh
-rw-rw-r--    1 aff-demo1 aff-demo1    90764 Dec 26 20:39 Form.pdf
-rw-r--r--    1 aff-demo1 aff-demo1     3219 Dec 26 20:38 wp-custom-theme-and-plugin-repo.md
sftp> pwd
Remote working directory: /home/aff-demo1
sftp> 
```



## Verify connection using FileZilla:

Create three different sites and provide your (same) SSH privte key (PPK) for all connections.



# Step 2 - SFTP Chroot the users into their home directories:

This way of setting up SFTP using SSH is not the same as configurations done in Step 1. The file and directory ownerships and permissions are completely different to get this done. 

You can also say that this way of configuration is mutually exclusive to Step 1. Setting up SFTP this way will result in users being jailed/chroot into their home directories when they use the SFTP service. The users will still be able to do a regular SSH login on the server, and they will **not** be chroot-ed into their home directories. If you want to stop users from doing anything over regular SSH session, you will need to change their login shell to something like `/bin/false` or `/sbin/nologin`. 

The file and directory permissions are special (and different) in this setup, and must be setup correctly to ensure security (isolation) between various SFTP accounts and non-SFTP (aka regular) accounts on the server.

Normally, any chroot environment expects that certain directories exist in the chrooted environment, such as `/bin`, `/dev`, etc. However, the `internal-sftp` sub-process/sub-system of SSH handles that internally, and that is why we don't need to create all these directories inside the user's home directory for chroot to work.

For this to work, we first need a separate `files` directory inside each user's home directory, and ensure that it is owned by the actual SFTP user. This is because in this type of setup, users will not be able to create any objects (files/directories) in the main home directory (`/home/aff-demo1`), instead they will only be able to read-write in this newly created sub-directory (`/home/aff-demo1/files`).

```
[root@SFTP-SERVER ~]# mkdir /home/aff-demo1/files

[root@SFTP-SERVER ~]# chown aff-demo1:aff-demo1 /home/aff-demo1/files

[root@SFTP-SERVER ~]# chmod 0700 /home/aff-demo1/files
```

The way Chroot works, the user home directory needs to be owned by user root, otherwise the chroot mechanism does not work. Note, only the top level directory (`/home/aff-demo1`) needs to be owned by `root`. The directories below that level must be owned by the actual SFTP user `aff-demo1`.

In addition to that, we need to set the permissions to `755` for each SFTP user's home directory (`/home/aff-demo1`) . This permission-set cannot be `700` and not `750`


```
[root@SFTP-SERVER ~]# chown root:root  /home/aff-demo1

[root@SFTP-SERVER ~]# chmod 0755 /home/aff-demo1
```


```
[root@SFTP-SERVER ~]# ls -l /home/
total 0
drwxr-xr-x  4 root      root      203 Dec 26 21:04 aff-demo1
drwx------  3 aff-demo2 aff-demo2 105 Dec 26 20:15 aff-demo2
drwx------  3 aff-demo3 aff-demo3 105 Dec 26 20:15 aff-demo3
drwx------. 3 centos    centos    105 Dec 26 19:27 centos
[root@SFTP-SERVER ~]# 
```

Next, we need to have a section in our SSHD config to chroot certain users to their home directories and force them to use `internal-sftp` process. Without this process the users won't be able to chroot to their home directories, and the SFTP session/connection will fail.

```
[root@SFTP-SERVER ~]# vi /etc/ssh/sshd_config 

. . . 

Subsystem       sftp    /usr/libexec/openssh/sftp-server

Match User aff-demo*
  ChrootDirectory /home/%u
  # ChrootDirectory %d
  ForceCommand internal-sftp
[root@SFTP-SERVER ~]# 
```

**Note:** It is absolutely critical to have the permissions set to `700` for `.ssh` and `files` directories for each user. Also, any files and directories under `/home/aff-demo1/` directory should generally have ownership set to the actual user `aff-demo1:aff-demo1`, and permissions to allow only the owner of the object (`aff-demo1`) to read/write/execute. Group and Others should have absolutely no permissions.

A successful chroot SFTP session:

```
[kamran@kworkhorse ~]$ sftp  aff-demo1@46.101.53.95
Connected to 46.101.53.95.
sftp> pwd
Remote working directory: /
sftp> ls -la
drwxr-xr-x    4 root     root          203 Dec 26 21:04 .
drwxr-xr-x    4 root     root          203 Dec 26 21:04 ..
-rw-------    1 aff-demo1 aff-demo1       14 Dec 26 20:24 .bash_history
-rw-------    1 aff-demo1 aff-demo1       18 Jul 27 14:21 .bash_logout
-rw-------    1 aff-demo1 aff-demo1      141 Jul 27 14:21 .bash_profile
-rw-------    1 aff-demo1 aff-demo1      376 Jul 27 14:21 .bashrc
-rw-------    1 aff-demo1 aff-demo1        0 Dec 26 20:05 .cloud-locale-test.skip
drwx------    2 aff-demo1 aff-demo1       29 Dec 26 20:37 .ssh
drwx------    2 aff-demo1 aff-demo1        6 Dec 26 21:04 files

sftp> cd files

sftp> ls -la
drwxr-xr-x    2 aff-demo1 aff-demo1       35 Dec 26 21:06 .
drwxr-xr-x    4 root     root          203 Dec 26 21:04 ..
-rw-r--r--    1 aff-demo1 aff-demo1    15620 Dec 26 21:06 wbitt-vm-to-docker.md

sftp> pwd
Remote working directory: /files
sftp> 
```


**Notes:** 
* Changing the shell of the SFTP user account from `/bin/bash` to `/bin/false` does not have any benefit in chroot SFTP environments. This is because the user is already being forced to use `interal-sftp` (the internal ftp sub-process provided by SSH). However, in this case, the user will be able to login to the server over regular SSH. To stop the user from doing that, the shell of the user should be changed to `/bin/false`  or `/sbin/nologin`. 
* Never force `root` to chroot. Use user `root` solely for regular system administration. Don't do anything fancy to user `root`.


## Adjust other user accounts for SFTP:

Time to setup other user accounts on the server to allow chroot SFTP . 

```
[root@SFTP-SERVER ~]# chown root:root /home/aff-demo*
[root@SFTP-SERVER ~]# ls -l /home/
total 0
drwxr-xr-x  4 root   root   139 Dec 26 21:16 aff-demo1
drwx------  3 root   root   126 Dec 26 21:12 aff-demo2
drwx------  3 root   root   126 Dec 26 21:12 aff-demo3
drwx------. 3 centos centos 105 Dec 26 19:27 centos
[root@SFTP-SERVER ~]# chmod 0755 /home/aff-demo* 
[root@SFTP-SERVER ~]# ls -la /home/
total 0
drwxr-xr-x.  6 root   root    71 Dec 26 20:14 .
dr-xr-xr-x. 17 root   root   244 Dec 26 20:05 ..
drwxr-xr-x   4 root   root   139 Dec 26 21:16 aff-demo1
drwxr-xr-x   3 root   root   126 Dec 26 21:12 aff-demo2
drwxr-xr-x   3 root   root   126 Dec 26 21:12 aff-demo3
drwx------.  3 centos centos 105 Dec 26 19:27 centos
[root@SFTP-SERVER ~]# 
```

```
[root@SFTP-SERVER ~]# mkdir /home/aff-demo2/files
[root@SFTP-SERVER ~]# mkdir /home/aff-demo3/files
[root@SFTP-SERVER ~]# chown aff-demo2:aff-demo2  /home/aff-demo2/files 
[root@SFTP-SERVER ~]# chown aff-demo3:aff-demo3  /home/aff-demo3/files
```

Verify:

```
[kamran@kworkhorse ~]$ sftp  aff-demo1@46.101.53.95
Connected to 46.101.53.95.

sftp> ls -l
drwxr-xr-x    2 aff-demo1 1001           67 Dec 26 21:15 files

sftp> pwd
Remote working directory: /

sftp> quit
```

```
[kamran@kworkhorse ~]$ sftp  aff-demo2@46.101.53.95
Connected to 46.101.53.95.

sftp> ls -l
drwxr-xr-x    2 aff-demo2 aff-demo2        6 Dec 26 21:20 files

sftp> pwd
Remote working directory: /

sftp> quit
```

```
[kamran@kworkhorse ~]$ sftp  aff-demo3@46.101.53.95
Connected to 46.101.53.95.
sftp> ls -l
drwxr-xr-x    2 aff-demo3 aff-demo3        6 Dec 26 21:20 files
sftp> pwd
Remote working directory: /
sftp> quit
[kamran@kworkhorse ~]$ 
```

