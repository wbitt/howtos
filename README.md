# Various HowTos, technical articles and videos by Kamran Azeem:

First, link to my public SSH keys: [https://gitlab.com/kamranazeem/ssh-public-keys](https://gitlab.com/kamranazeem/ssh-public-keys)


## Cloud (AWS):
* [Learn AWS](https://github.com/KamranAzeem/learn-aws)

## Docker:
* [Learn Docker](https://github.com/KamranAzeem/learn-docker)
* [Deploy to Docker using CI/CD](https://gitlab.com/wbitt/deploy2docker)
* [Docker to Kubernetes](https://github.com/KamranAzeem/docker-to-kubernetes)

## Docker based projects:
* [Network Multitool container image](https://github.com/wbitt/Network-MultiTool)
* [SSH server in a container](https://gitlab.com/kamranazeem/ssh-server)
* [Backup MySQL database from inside MySQL container](https://gitlab.com/wbitt/docker/mysql-backup)

## Kubernetes:
* [Kubernetes Katas](https://github.com/KamranAzeem/kubernetes-katas)
* [Learn Kubernetes](https://github.com/KamranAzeem/learn-kubernetes)

## CI/CD:
* [Gitlab CI demo](https://gitlab.com/kamranazeem/gitlab-ci-demo)

## General / Miscellaneous:
* [Learn LetsEncrypt](https://github.com/KamranAzeem/learn-letsencrypt)
* [Chroot SFTP setup on CENTOS 8](chroot-sftp-setup-on-centos-8.md)
* [OpenVPN setup on CENTOS 8 on DigitalOcean - with Floating IP](openvpn-on-centos-8-digitalocean-with-floating-ip.md)
* [Development on a shared mac computer](https://gitlab.com/kamranazeem/development-on-shared-mac)
* [Home studio setup](https://gitlab.com/kamranazeem/home-studio)
* [LibVirt/KVM - power up/down script for VMs](https://gitlab.com/kamranazeem/LibVirt)
* [Learn PHP](https://gitlab.com/kamranazeem/learn-php)
* [Various system administration scripts](https://gitlab.com/wbitt/admin-scripts)
